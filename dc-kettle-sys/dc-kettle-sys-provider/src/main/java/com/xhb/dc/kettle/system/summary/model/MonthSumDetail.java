package com.xhb.dc.kettle.system.summary.model;

import lombok.Data;

/**
 * MonthSumDetail.
 */
@Data
public class MonthSumDetail {

    private int cnt;

    private String mon;

}
